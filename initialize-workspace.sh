#!/bin/bash

sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
sudo apt update
sudo apt install ros-melodic-desktop-full

sudo apt install net-tools autoconf automake libtool cmake bison re2c build-essential g++ gitk uuid-dev python-catkin-tools

git clone https://github.com/potassco/clingo.git
cd clingo/
mkdir build
git submodule update --init --recursive
cd build
cmake ..
make
sudo make install

cd ../..

mkdir src
catkin init

cd src

git clone https://github.com/dasys-lab/aspsuite.git
git clone https://github.com/dasys-lab/essentials.git
git clone git@bitbucket.org:sjakob872/asp_ontology.git

cd aspsuite
git checkout asp_dev
cd ..

cd essentials
git checkout ttb_dev
cd ..

echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
echo "source ~/workspace/devel/setup.bash" >> ~/.bashrc
echo "export DOMAIN_CONFIG_FOLDER=\"/home/HOME_FOLDER_NAME/workspace/src/asp_ontology/etc\"" >> ~/.bashrc

source ~/.bashrc

cd ..

#catkin build
