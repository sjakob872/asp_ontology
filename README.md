# README #

This repository presents the work in progress for an automatic extraction of a commonesence ontology from a locally hosted [ConceptNet5](https://conceptnet.io) instance and its translation into Answer Set Programming.

### How do I get set up? ###

A guide for installing a local instance of ConceptNet5 can be found [here](https://github.com/commonsense/conceptnet5/wiki/Build-process). (Requires roughly 250 GB disk space and more than 30 GB of RAM)

In order to compile the Code, a Linux system (tested on Ubuntu 18.04) is needed.
The installation is started by running the initialize_workspace.sh script. Please create a folder named **workspace** in your home folder, copy the script into this folder, make it executable, and 
replace **HOME_FOLDER_NAME** in line 40 with you current home folder name. Afterwards, run the script in the **workspace** folder.
It will install [ROS](https://www.ros.org/) and neccessary dependencies. After the successfull exectuion of the script, navigate to the folder **src/asp_ontology**. The repository can be build by running **catkin build --this**.
To enable ROS to find the package and the executable, run **source ~/.bashrc**.

Several example ontologies can be found in the etc folder of this repository.
To use the existing ontologies, execute **rosrun cn5_ontology_extractor cn5_ontology_extractor_solve_test**. 
