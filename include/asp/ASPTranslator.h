#pragma once

#include "conceptnet/Concept.h"
#include <map>
#include <string>

namespace conceptnet
{
class Edge;
class Concept;
} // namespace conceptnet
namespace cnoe
{
class AnswerGraph;
class Ontology;
} // namespace cnoe
namespace asp
{
class ASPTranslator
{
public:
    enum InconsistencyRemoval
    {
        None,
        KeepHighestWeight,
        UseExternals
    };
    ASPTranslator();
    cnoe::Ontology* extractOntology(cnoe::AnswerGraph* answerGraph, bool createExternals = false);

    std::string generateOntologyRules(bool addShow);

    static const std::string CONCEPTNET_PREFIX;
    static const std::string SUB_SET_OF;
    static const std::string SUB_SET_OF_INTERNAL;
    static const std::string CLASSIFIED_AS;
    static const std::string CLASSIFIED_AS_INTERNAL;
    static const std::string PROGRAM_SECTION;
    static const std::string INPUT;

private:
    cnoe::Ontology* createOntology(
            cnoe::AnswerGraph* answerGraph, bool createExternals = false);
    void generateSupportOntologyRules(std::string& program);
    void generateSubSetRules(std::string& program);
    void generateClassifiedRules(std::string& program);
    void generateShowStatements(std::string& program);

    std::string conceptToASPPredicate(std::string concept);

    std::string lowerIsARelation;
    std::string lowerFormOfRelation;
    std::string lowerSynonymRelation;
};

} // namespace asp