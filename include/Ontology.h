#pragma once

#include <string>
#include <vector>

namespace cnoe
{
class Ontology
{
public:
    Ontology() = default;
    std::string ontology;
    std::vector<std::string> externals;
    std::string rules;
};
} // namespace cnoe
