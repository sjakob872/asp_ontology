#pragma once

#include <string>

//#define ONTOLOGY_DEBUG

namespace asp
{
class ASPTranslator;
}
namespace conceptnet
{
class ConceptNet;
}
namespace cnoe
{
class Ontology;
class AnswerGraph;
class OntologyExtractor
{
public:
    OntologyExtractor();
    ~OntologyExtractor();
    void renderDot(cnoe::AnswerGraph* answerGraph);
    std::string readFromFile(std::string path);

    AnswerGraph* extractOntology(std::string input);
    Ontology* generateASPOntology(AnswerGraph* answerGraph);

    static const double IS_A_WEIGHT;
    static const double FORM_OF_WEIGHT;
    static const double SYNONYM_WEIGHT;
    static const double MIN_RELATEDNESS;

private:
    asp::ASPTranslator* translator;
    conceptnet::ConceptNet* conceptNet;
};
} // namespace cnoe
