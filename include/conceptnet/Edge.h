#pragma once

#include "conceptnet/Relations.h"

#include <string>
#include <vector>


namespace conceptnet
{
class Concept;
class Edge
{
public:
    Edge(std::string id, std::string language, Concept* fromConcept, Concept* toConcept, conceptnet::Relation relation, double weight);
    virtual ~Edge();

    std::string id;
    std::string language;
    Concept* fromConcept;
    Concept* toConcept;
    conceptnet::Relation relation;
    double weight;
    double level;
    double relatedness;
    bool causesInconsistency = false;

    std::string toString(std::string indent = "") const;

    conceptnet::Concept* getOpposite(conceptnet::Concept* concept);

    bool operator<(const conceptnet::Edge& another);
};
} // namespace container

bool operator==(const conceptnet::Edge& one, const conceptnet::Edge& another);

