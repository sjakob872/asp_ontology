#include "OntologyExtractor.h"
#include "AnswerGraph.h"
#include "Ontology.h"
#include "asp/ASPTranslator.h"
#include "conceptnet/ConceptNet.h"
#include <cgraph.h>
#include <chrono>
#include <gvc.h>
#include <gvcext.h>
#include <iostream>
#include <unordered_set>
#include <fstream>

const double cnoe::OntologyExtractor::IS_A_WEIGHT = 2.0;
const double cnoe::OntologyExtractor::FORM_OF_WEIGHT = 2.0;
const double cnoe::OntologyExtractor::SYNONYM_WEIGHT = 2.0;
const double cnoe::OntologyExtractor::MIN_RELATEDNESS = 0.7;

namespace cnoe
{
OntologyExtractor::OntologyExtractor()
{
    this->translator = new asp::ASPTranslator();
    this->conceptNet = new conceptnet::ConceptNet();
}

OntologyExtractor::~OntologyExtractor()
{
    delete this->translator;
    delete this->conceptNet;
}

void OntologyExtractor::renderDot(cnoe::AnswerGraph* answerGraph)
{
    Agraph_t* g;
    /* set up a graphviz context - but only once even for multiple graphs */
    static GVC_t* gvc;
    if (!gvc) {
        gvc = gvContext();
    }
    /* Create a simple digraph */
    g = agopen("cn5_Ontology", Agdirected, nullptr);
    agsafeset(g, "rankdir", "RL", "");
    answerGraph->renderDot(g, false);

    /* Set an attribute - in this case one that affects the visible rendering */
    /* Use the directed graph layout engine */
    gvLayout(gvc, g, "dot");
    /* Output in .dot format */
    FILE* fptr;
    fptr = fopen("test.dot", "w");
    gvRender(gvc, g, "dot", fptr);
    fclose(fptr);
    gvFreeLayout(gvc, g);
    agclose(g);

    // call this to translate into ps format and open with evince
    //    dot -Tps ~/test.dot -o outfile.ps
}

std::string OntologyExtractor::readFromFile(std::string path)
{
    std::ifstream file;
    file.open(path);

    if (!file) {
        std::cerr << "Unable to open file" << std::endl;
        exit(EXIT_FAILURE); // call system to stop
    }
    std::string ret;
    std::string tmp;
    while (!file.eof()) // To get you all the lines.
    {
        getline(file, tmp);
        ret.append(tmp);
    }
    file.close();
    return ret;
}

AnswerGraph* OntologyExtractor::extractOntology(std::string input)
{
#ifdef ONTOLOGY_DEBUG
    int counter = 0;
#endif
    double level = 0.0;
    auto start = std::chrono::high_resolution_clock::now();
    cnoe::AnswerGraph* answerGraph = new cnoe::AnswerGraph();
    conceptnet::Concept* root = this->conceptNet->getConcept(answerGraph, input);
    answerGraph->setRoot(root);
    std::vector<conceptnet::Edge*> edges;
    std::unordered_set<conceptnet::Concept*> visited;
    std::vector<conceptnet::Concept*> firstFringe;
    std::set<conceptnet::Concept*> secondFringe;
    conceptnet::Concept* currentConcept;
    firstFringe.push_back(root);
    std::cout << "Starting to crawl CN5." << std::endl;
    do {
        secondFringe.clear();
        while (!firstFringe.empty()) {
            currentConcept = firstFringe.front();
            edges = this->conceptNet->getEdges(answerGraph, conceptnet::Relation::IsA, currentConcept->term, -1, OntologyExtractor::IS_A_WEIGHT);
            auto tmp = this->conceptNet->getEdges(answerGraph, conceptnet::Relation::FormOf, currentConcept->term, -1, OntologyExtractor::FORM_OF_WEIGHT);
            edges.insert(edges.end(), tmp.begin(), tmp.end());
            currentConcept->addEdges(edges);
            visited.insert(currentConcept);
#ifdef ONTOLOGY_DEBUG
            if (counter < 2) {
#endif
                for (conceptnet::Edge* edge : edges) {
                    if (visited.find(edge->fromConcept) == visited.end() || visited.find(edge->toConcept) == visited.end()) {
                        if (std::find(secondFringe.begin(), secondFringe.end(), edge->fromConcept) == secondFringe.end()) {
                            if(edge->level < level) {
                                edge->level = level;
                            }
                            edge->relatedness = this->conceptNet->getRelatedness(edge->fromConcept->term, edge->toConcept->term);
                            secondFringe.insert(edge->fromConcept);
                            secondFringe.insert(edge->toConcept);
                        }
                    }
#ifdef ONTOLOGY_DEBUG
                }
#endif
            }
            firstFringe.erase(firstFringe.begin());
#ifdef ONTOLOGY_DEBUG
            counter++;
#endif
        }
        //level = level >= 2.0 ? level - 2.0 : 0.0;
        firstFringe.insert(firstFringe.begin(), secondFringe.begin(), secondFringe.end());
    } while (!secondFringe.empty());

    for (auto pair : answerGraph->getConcepts()) {
        auto edges = this->conceptNet->getEdges(answerGraph, conceptnet::Relation::Synonym, pair.second->term, -1, OntologyExtractor::SYNONYM_WEIGHT);
        for(conceptnet::Edge* edge : edges) {
            double relatedness = this->conceptNet->getRelatedness(edge->fromConcept->term, edge->toConcept->term);
            if(relatedness < OntologyExtractor::MIN_RELATEDNESS) {
                edge->relatedness = -1;
                continue;
            }
            edge->relatedness = relatedness;
            pair.second->addEdge(edge);
        }
    }

   /* for(auto edge : answerGraph->getEdges()) {
        edge.second->relatedness = this->conceptNet->getRelatedness(edge.second->fromConcept->term, edge.second->toConcept->term);
    }*/

    auto finish = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    std::cout << "Ontology extraction runtime: " << duration / 1000.0 << " s" << std::endl;

    return answerGraph;
}

Ontology* OntologyExtractor::generateASPOntology(cnoe::AnswerGraph* answerGraph)
{
    std::string domain_config_folder = ::getenv("DOMAIN_CONFIG_FOLDER");

    std::cout << "Finished crawling CN5. Starting to write LP." << std::endl;

    auto start = std::chrono::high_resolution_clock::now();
    cnoe::Ontology* ontology = this->translator->extractOntology(answerGraph, true);
    ontology->rules = this->translator->generateOntologyRules(true);

    auto finish = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    std::cout << "Ontology creation runtime: " << duration / 1000.0 << " ms" << std::endl;

    std::ofstream file;
    file.open(domain_config_folder + "/ontologies/test/ontology.lp");
    file << ontology->ontology;
    file << ontology->rules;
    file.close();

    std::cout << "Finished writing LP." << std::endl;

    // conceptNet->findInconsistencies(answerGraph);
    //renderDot(answerGraph);
    // system("dot -Tps test.dot -o outfile.ps");
    //system("dot -Tpdf test.dot -o outfile.pdf");

    return ontology;
}

} // namespace cnoe