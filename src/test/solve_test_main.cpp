#include <chrono>
#include <fstream>
#include <reasoner/asp/Solver.h>

std::string readFromFile(std::string path)
{
    std::ifstream file;
    file.open(path);

    if (!file) {
        std::cerr << "Unable to open file" << std::endl;
        exit(EXIT_FAILURE); // call system to stop
    }
    std::string ret;
    std::string tmp;
    while (!file.eof()) // To get you all the lines.
    {
        getline(file, tmp);
        ret.append(tmp);
    }
    file.close();
    return ret;
}

int main(int argc, char** argv)
{

    std::string domain_config_folder = ::getenv("DOMAIN_CONFIG_FOLDER");
    std::string programSection = "commonsenseOntology";

    std::string ontology = readFromFile(domain_config_folder + "/ontologies/test/ontology2.lp");

    std::string facts = readFromFile(domain_config_folder + "/ontologies/test/test_facts.lp");

    reasoner::asp::Solver* solver = new reasoner::asp::Solver({});

    /**
     * ################# Ontology #################
     */

    auto start = std::chrono::high_resolution_clock::now();
    solver->add(programSection.c_str(), {}, (ontology + facts).c_str());
    auto finish = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    std::cout << "Adding runtime: " << duration / 1000.0 << " ms" << std::endl;

    start = std::chrono::high_resolution_clock::now();
    solver->ground({{programSection.c_str(), {}}}, nullptr);
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    std::cout << "Grounding runtime: " << duration / 1000.0 << " ms" << std::endl;

    start = std::chrono::high_resolution_clock::now();
    solver->solve();
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    std::cout << "Solving runtime: " << duration / 1000.0 << " ms" << std::endl;

    auto models = solver->getCurrentModels();
    std::stringstream ss;
    for (int i = 0; i < models.size(); i++) {
        ss << "Model number " << i + 1 << ":\n" << std::endl;
        for (auto atom : models.at(i)) {
            ss << atom << " ";
        }
        ss << std::endl;
    }

    std::cout << "Solution: " << ss.str() << std::endl;
}