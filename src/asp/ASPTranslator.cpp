#include "asp/ASPTranslator.h"

#include "AnswerGraph.h"
#include "Ontology.h"
#include "conceptnet/Concept.h"
#include "conceptnet/Edge.h"
#include "conceptnet/Relations.h"

#include <algorithm>
#include <vector>

namespace asp
{

const std::string ASPTranslator::CONCEPTNET_PREFIX = "cs_";
const std::string ASPTranslator::SUB_SET_OF = "subSetOf";
const std::string ASPTranslator::SUB_SET_OF_INTERNAL = "subSetOfInternal";
const std::string ASPTranslator::CLASSIFIED_AS = "classifiedAs";
const std::string ASPTranslator::CLASSIFIED_AS_INTERNAL = "classifiedAsInternal";
const std::string ASPTranslator::PROGRAM_SECTION = "commonsenseOntology";
const std::string ASPTranslator::INPUT = "is";

ASPTranslator::ASPTranslator()
{
    this->lowerIsARelation = conceptnet::relations[conceptnet::Relation::IsA];
    this->lowerIsARelation[0] = std::tolower(this->lowerIsARelation[0]);
    this->lowerFormOfRelation = conceptnet::relations[conceptnet::Relation::FormOf];
    this->lowerFormOfRelation[0] = std::tolower(this->lowerFormOfRelation[0]);
    this->lowerSynonymRelation = conceptnet::relations[conceptnet::Relation::Synonym];
    this->lowerSynonymRelation[0] = std::tolower(this->lowerSynonymRelation[0]);
}

cnoe::Ontology* ASPTranslator::extractOntology(cnoe::AnswerGraph* answerGraph, bool createExternals)
{
    if (answerGraph->getEdges().empty()) {
        return nullptr;
    }
    return createOntology(answerGraph, createExternals);
}

std::string ASPTranslator::generateOntologyRules(bool addShow)
{
    std::string program;
    generateSupportOntologyRules(program);
    generateSubSetRules(program);
    generateClassifiedRules(program);

    if (addShow) {
        generateShowStatements(program);
    }
    return program;
}

void ASPTranslator::generateShowStatements(std::string& program)
{
    program.append("#show subSetOf/4.\n");
    program.append("#show classifiedAs/3.\n");
    program.append("#show is/2.\n");
}

void ASPTranslator::generateSupportOntologyRules(std::string& program)
{
    /**
     * isA
     */
    // isA(X, Y, W) :- is(X, Z), cs_isA(Z, Y, W).
    program.append(this->lowerIsARelation + "(X, Y, W)")
            .append(" :- " + ASPTranslator::INPUT + "(X, Z), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerIsARelation + "(Z, Y, W).\n");
    // isA(X, Z, W) :- is(X, Z), cs_isA(Z, Y, W).
    program.append(this->lowerIsARelation + "(X, Z, W)")
            .append(" :- " + ASPTranslator::INPUT + "(X, Z), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerIsARelation + "(Z, Y, W).\n");
    // isA(X, Y, W) :- isA(X, Z, V), cs_isA(Z, Y, W), V < W.
    program.append(this->lowerIsARelation + "(X, Y, W)")
            .append(" :- " + this->lowerIsARelation + "(X, Z, V), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerIsARelation + "(Z, Y, W), V < W.\n");
    // isA(X, Z, W) :- formOf(X, Y, _), cs_isA(Y, Z, W), cs_formOf(V, Y, _).
    program.append(this->lowerIsARelation + "(X, Z, W)")
            .append(" :- " + this->lowerFormOfRelation + "(X, Y, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerIsARelation + "(Y, Z, W), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(V, Y, _).\n");
    // isA(X, Z, W) :- synonym(X, Z, V), cs_isA(Z, Y, W), V < W.
    program.append(this->lowerIsARelation + "(X, Z, W)")
            .append(" :- " + this->lowerSynonymRelation + "(X, Z, V), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerIsARelation + "(Z, Y, W), V < W.\n\n");
    /**
     * formOf
     */
    // formOf(X, Y, W) :- is(X, Z), cs_formOf(Z, Y, W).
    program.append(this->lowerFormOfRelation + "(X, Y, W)")
            .append(" :- " + ASPTranslator::INPUT + "(X, Z), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(Z, Y, W).\n");
    // formOf(X, Z, W) :- is(X, Z), cs_formOf(Z, Y, W).
    program.append(this->lowerFormOfRelation + "(X, Z, W)")
            .append(" :- " + ASPTranslator::INPUT + "(X, Z), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(Z, Y, W).\n");
    // formOf(X, Y, W) :- formOf(X, Z, V), cs_formOf(Z, Y, W), V < W.
    program.append(this->lowerFormOfRelation + "(X, Y, W)")
            .append(" :- " + this->lowerFormOfRelation + "(X, Z, V), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(Z, Y, W), V < W.\n");
    // formOf(X, Z, W) :- isA(X, Y, _), cs_formOf(Y, Z, W), cs_isA(V, Y, _).
    program.append(this->lowerFormOfRelation + "(X, Z, W)")
            .append(" :- " + this->lowerIsARelation + "(X, Y, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(Y, Z, W), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerIsARelation + "(V, Y, _).\n");
    // formOf(X, Z, W) :- synonym(X, Z, V), cs_formOf(Z, Y, W), V < W.
    program.append(this->lowerFormOfRelation + "(X, Z, W)")
            .append(" :- " + this->lowerSynonymRelation + "(X, Z, V), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(Z, Y, W), V < W.\n\n");

    /**
     * synonym
     */
    // synonym(X, Y, W) :- is(X, Z), cs_synonym(Z, Y, W).
    program.append(this->lowerSynonymRelation + "(X, Y, W)")
            .append(" :- " + ASPTranslator::INPUT + "(X, Z), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(Z, Y, W).\n");
    // synonym(X, Z, W) :- is(X, Z), cs_synonym(Z, Y, W).
    program.append(this->lowerSynonymRelation + "(X, Z, W)")
            .append(" :- " + ASPTranslator::INPUT + "(X, Z), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(Z, Y, W).\n");
    // synonym(X, Y, W) :- isA(X, Z, _), cs_synonym(Z, Y, W).
    program.append(this->lowerSynonymRelation + "(X, Y, W)")
            .append(" :- " + this->lowerIsARelation + "(X, Z, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(Z, Y, W).\n");
    // synonym(Y, X, W) :- isA(X, Z, _), cs_synonym(Y, Z, W).
    program.append(this->lowerSynonymRelation + "(Y, X, W)")
            .append(" :- " + this->lowerIsARelation + "(X, Z, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(Y, Z, W).\n");
    // synonym(X, Y, W) :- formOf(X, Z, _), cs_synonym(Z, Y, W).
    program.append(this->lowerSynonymRelation + "(X, Y, W)")
            .append(" :- " + this->lowerFormOfRelation + "(X, Z, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(Z, Y, W).\n");
    // synonym(Y, X, W) :- formOf(X, Z, _), cs_synonym(Y, Z, W).
    program.append(this->lowerSynonymRelation + "(Y, X, W)")
            .append(" :- " + this->lowerFormOfRelation + "(X, Z, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(Y, Z, W).\n\n");
}

void ASPTranslator::generateSubSetRules(std::string& program)
{
    // subSetOfInternal(X, Y, is, 1) :- is(X, Y).
    program.append(ASPTranslator::SUB_SET_OF_INTERNAL + "(X, Y, " + ASPTranslator::INPUT + ", 1)")
            .append(" :- " + ASPTranslator::INPUT + "(X, Y).\n\n");

    // subSetOfInternal(Y, X, formOf, W) :- formOf(Z, X, _), formOf(Z, Y, _), cs_formOf(Y, X, W).
    program.append(ASPTranslator::SUB_SET_OF_INTERNAL + "(Y, X, " + this->lowerFormOfRelation + ", W)")
            .append(" :- " + this->lowerFormOfRelation + "(Z, X, _), ")
            .append(this->lowerFormOfRelation + "(Z, Y, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(Y, X, W).\n");
    // subSetOfInternal(Y, X, formOf, W) :- formOf(Z, X, _), isA(Z, Y, _), cs_isA(Y, X, W).
    program.append(ASPTranslator::SUB_SET_OF_INTERNAL + "(Y, X, " + this->lowerFormOfRelation + ", W)")
            .append(" :- " + this->lowerFormOfRelation + "(Z, X, _), ")
            .append(this->lowerIsARelation + "(Z, Y, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerIsARelation + "(Y, X, W).\n");
    // subSetOfInternal(Y, X, formOf, W) :- formOf(Z, X, _), synonym(Z, Y, _), cs_formOf(Y, X, W).
    program.append(ASPTranslator::SUB_SET_OF_INTERNAL + "(Y, X, " + this->lowerFormOfRelation + ", W)")
            .append(" :- " + this->lowerFormOfRelation + "(Z, X, _), ")
            .append(this->lowerSynonymRelation + "(Z, Y, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(Y, X, W).\n\n");

    // subSetOfInternal(Y, X, isA, W) :- isA(Z, X, _), isA(Z, Y, _), cs_isA(Y, X, W).
    program.append(ASPTranslator::SUB_SET_OF_INTERNAL + "(Y, X, " + this->lowerIsARelation + ", W)")
            .append(" :- " + this->lowerIsARelation + "(Z, X, _), ")
            .append(this->lowerIsARelation + "(Z, Y, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerIsARelation + "(Y, X, W).\n");
    // subSetOfInternal(Y, X, isA, W) :- isA(Z, X, _), formOf(Z, Y, _), cs_isA(Y, X, W).
    program.append(ASPTranslator::SUB_SET_OF_INTERNAL + "(Y, X, " + this->lowerIsARelation + ", W)")
            .append(" :- " + this->lowerIsARelation + "(Z, X, _), ")
            .append(this->lowerFormOfRelation + "(Z, Y, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerIsARelation + "(Y, X, W).\n");
    // subSetOfInternal(Y, X, isA, W) :- isA(Z, X, _), synonym(Z, Y, _), cs_isA(Y, X, W).
    program.append(ASPTranslator::SUB_SET_OF_INTERNAL + "(Y, X, " + this->lowerIsARelation + ", W)")
            .append(" :- " + this->lowerIsARelation + "(Z, X, _), ")
            .append(this->lowerSynonymRelation + "(Z, Y, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerIsARelation + "(Y, X, W).\n\n");

    // subSetOfInternal(Y, X, synonym, W) :- synonym(Z, X, _), cs_isA(X, _, W), cs_synonym(Y, X, _).
    program.append(ASPTranslator::SUB_SET_OF_INTERNAL + "(Y, X, " + this->lowerSynonymRelation + ", W)")
            .append(" :- " + this->lowerSynonymRelation + "(Z, X, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerIsARelation + "(X, _, W), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(Y, X, _).\n");
    // subSetOfInternal(Y, X, synonym, W) :- synonym(Z, X, _), cs_formOf(X, _, W), cs_synonym(Y, X, _).
    program.append(ASPTranslator::SUB_SET_OF_INTERNAL + "(Y, X, " + this->lowerSynonymRelation + ", W)")
            .append(" :- " + this->lowerSynonymRelation + "(Z, X, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(X, _, W), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(Y, X, _).\n");
    // subSetOfInternal(Y, X, synonym, W) :- synonym(Z, X, _), cs_synonym(Y, X, W).
    program.append(ASPTranslator::SUB_SET_OF_INTERNAL + "(Y, X, " + this->lowerSynonymRelation + ", W)")
            .append(" :- " + this->lowerSynonymRelation + "(Z, X, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(Y, X, W).\n");
    // subSetOfInternal(Y, X, synonym, W) :- synonym(Y, Z, _), cs_synonym(X, Y, W).
    program.append(ASPTranslator::SUB_SET_OF_INTERNAL + "(Y, X, " + this->lowerSynonymRelation + ", W)")
            .append(" :- " + this->lowerSynonymRelation + "(Y, Z, _), ")
            .append(ASPTranslator::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(X, Y, W).\n");
    // subSetOf(X, Y, R, Z) :- Z = #max{ W : subSetOfInternal(X, Y, R, W)}, subSetOfInternal(X, Y, R, _).
    program.append(ASPTranslator::SUB_SET_OF + "(X, Y, R, Z)")
            .append(" :- Z = #max{ W : " + ASPTranslator::SUB_SET_OF_INTERNAL + "(X, Y, R, W)}, ")
            .append(ASPTranslator::SUB_SET_OF_INTERNAL + "(X, Y, R, _).\n\n");
}

void ASPTranslator::generateClassifiedRules(std::string& program)
{
    // classifiedAsInternal(X, Y, 1) :- is(X, Y).
    program.append(ASPTranslator::CLASSIFIED_AS_INTERNAL + "(X, Y, 1)")
            .append(" :-" + ASPTranslator::INPUT + "(X, Y).\n");
    // classifiedAsInternal(X, Y, Z) :- Z = #max{ W : synonym(X, Y, W)}, synonym(X, Y, _), is(X, _).
    program.append(ASPTranslator::CLASSIFIED_AS_INTERNAL + "(X, Y, Z)")
            .append(" :- Z = #max{ W : " + this->lowerSynonymRelation + "(X, Y, W)}, ")
            .append(this->lowerSynonymRelation + "(X, Y, _), ")
            .append(ASPTranslator::INPUT + "(X, _).\n");
    // classifiedAsInternal(X, Y, Z) :- Z = #max{ W : isA(X, Y, W)}, isA(X, Y, _), is(X, _).
    program.append(ASPTranslator::CLASSIFIED_AS_INTERNAL + "(X, Y, Z)")
            .append(" :- Z = #max{ W : " + this->lowerIsARelation + "(X, Y, W)}, ")
            .append(this->lowerIsARelation + "(X, Y, _), ")
            .append(ASPTranslator::INPUT + "(X, _).\n");
    // classifiedAsInternal(X, Y, Z) :- Z = #max{ W : formOf(X, Y, W)}, formOf(X, Y, _), is(X, _).
    program.append(ASPTranslator::CLASSIFIED_AS_INTERNAL + "(X, Y, Z)")
            .append(" :- Z = #max{ W : " + this->lowerFormOfRelation + "(X, Y, W)}, ")
            .append(this->lowerFormOfRelation + "(X, Y, _), ")
            .append(ASPTranslator::INPUT + "(X, _).\n");
    // classifiedAs(X, Y, Z) :- Z = #max{ W : classifiedAsInternal(X, Y, W)}, classifiedAsInternal(X, Y, _).
    program.append(ASPTranslator::CLASSIFIED_AS + "(X, Y, Z)")
            .append(" :- Z = #max{ W : " + ASPTranslator::CLASSIFIED_AS_INTERNAL + "(X, Y, W)}, ")
            .append(ASPTranslator::CLASSIFIED_AS_INTERNAL + "(X, Y, _).\n\n");
}

std::string ASPTranslator::conceptToASPPredicate(std::string concept)
{
    std::replace(concept.begin(), concept.end(), '.', '_');
    std::replace(concept.begin(), concept.end(), ',', '_');
    std::replace(concept.begin(), concept.end(), ' ', '_');
    return concept;
}

cnoe::Ontology* ASPTranslator::createOntology(cnoe::AnswerGraph* answerGraph, bool createExternals)
{

    cnoe::Ontology* ret = new cnoe::Ontology();
    std::string program;

    std::vector<conceptnet::Concept*> openNodes;
    std::vector<conceptnet::Concept*> closedNodes;
    std::vector<conceptnet::Edge*> translatedEdges;

    openNodes.push_back(answerGraph->root);

    while (!openNodes.empty()) {
        conceptnet::Concept* node = openNodes[0];
        // std::cout << "AnswerGraph:renderDot: " << node->term << " " << node << std::endl;
        openNodes.erase(openNodes.begin());
        if (std::find(closedNodes.begin(), closedNodes.end(), node) != closedNodes.end()) {
            continue;
        }
        closedNodes.push_back(node);

        for (conceptnet::Edge* edge : node->getEdges()) {
            if (std::find(translatedEdges.begin(), translatedEdges.end(), edge) != translatedEdges.end()) {
                continue;
            }
            translatedEdges.push_back(edge);
            std::string tmp = "";
            std::string lowerRelation = conceptnet::relations[edge->relation];
            lowerRelation[0] = std::tolower(lowerRelation[0]);
            tmp.append(ASPTranslator::CONCEPTNET_PREFIX).append(lowerRelation);
            tmp.append("(\"")
                    .append(conceptToASPPredicate(edge->fromConcept->term))
                    .append("\", \"")
                    .append(conceptToASPPredicate(edge->toConcept->term))
                    .append("\", ")
                    .append(std::to_string((int) ((edge->weight + edge->level + edge->relatedness) * 100.0)))
                    .append(")");
            if (createExternals) {
                ret->externals.push_back(tmp);
                tmp = "#external " + tmp;
            }
            tmp.append(".\n");
            program.append(tmp);
            openNodes.push_back(edge->toConcept);
            openNodes.push_back(edge->fromConcept);
        }
    }
    program.append("\n");
    ret->ontology = program;
    return ret;
}
} // namespace asp