#include "AnswerGraph.h"
#include "Ontology.h"
#include "OntologyExtractor.h"
#include <chrono>
#include <cstdlib>
#include <reasoner/asp/Solver.h>

//#define ONTOLOGY_DEBUG
int main(int argc, char** argv)
{
    if (argc < 2) {
        std::cout << "Input Concept!" << std::endl;
        return EXIT_FAILURE;
    }

    cnoe::OntologyExtractor* ontologyExtractor = new cnoe::OntologyExtractor();

    std::string domain_config_folder = ::getenv("DOMAIN_CONFIG_FOLDER");
    std::string facts = ontologyExtractor->readFromFile(domain_config_folder + "/ontologies/test/test_facts.lp");

    cnoe::AnswerGraph* answerGraph = ontologyExtractor->extractOntology(argv[1]);
    cnoe::Ontology* ontology = ontologyExtractor->generateASPOntology(answerGraph);

    /**
     * ################# ADD #################
     */

    auto start = std::chrono::high_resolution_clock::now();
    reasoner::asp::Solver* solver = new reasoner::asp::Solver({});

    solver->add("", {}, ontology->ontology.c_str());

    auto finish = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    std::cout << "Adding runtime: " << duration / 1000.0 << " ms" << std::endl;

    /**
     * ################# Ground #################
     */

    start = std::chrono::high_resolution_clock::now();
    solver->ground({{"", {}}}, nullptr);
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    std::cout << "Grounding runtime: " << duration / 1000.0 << " ms" << std::endl;

    /**
     * ################# Assign Externals #################
     */

    start = std::chrono::high_resolution_clock::now();
    for (auto external : ontology->externals) {
        solver->assignExternal(solver->parseValue(external.c_str()), Clingo::TruthValue::True);
    }
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    std::cout << "External runtime: " << duration / 1000.0 << " ms" << std::endl;

    /**
     * ################# Solve #################
     */
    start = std::chrono::high_resolution_clock::now();
    solver->solve();
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    std::cout << "Solving runtime: " << duration / 1000.0 << " ms" << std::endl;

    /**
     * ################# Query #################
     */

    start = std::chrono::high_resolution_clock::now();
    solver->add("test", {}, (ontology->rules + facts).c_str());
    solver->ground({{"test", {}}}, nullptr);
    solver->solve();
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    std::cout << "Query runtime: " << duration / 1000.0 << " ms" << std::endl;

    auto models = solver->getCurrentModels();
    std::stringstream ss;
    for (size_t i = 0; i < models.size(); i++) {
        ss << "Model number " << i + 1 << ":\n" << std::endl;
        for (auto atom : models.at(i)) {
            ss << atom << " ";
        }
        ss << std::endl;
    }
    std::cout << "Solution: " << ss.str() << std::endl;

    delete ontologyExtractor;
    delete ontology;
    delete solver;
    delete answerGraph;
    return EXIT_SUCCESS;
}